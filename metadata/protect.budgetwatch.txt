Categories:Money
License:GPLv3+
Web Site:
Source Code:https://github.com/brarcher/budget-watch
Issue Tracker:https://github.com/brarcher/budget-watch/issues

Auto Name:Budget Watch
Summary:Help manage your personal budget
Description:
Budget Watch helps manage personal budgets. After adding your budgets, simply
record your day-to-day transactions. You can then view how close your spending
is to your budget.
.

Repo Type:git
Repo:https://github.com/brarcher/budget-watch

Build:0.1,1
    commit=v0.1
    subdir=app
    gradle=yes

Build:0.2,2
    commit=v0.2
    subdir=app
    gradle=yes

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:0.2
Current Version Code:2
