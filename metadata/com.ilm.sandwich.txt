Categories:Navigation
License:Apache2
Web Site:http://smartnavi-app.com/
Source Code:https://github.com/Phantast/smartnavi
Issue Tracker:https://github.com/Phantast/smartnavi/issues

Auto Name:SmartNavi
Summary:Navigation for pedestrians
Description:
Step based and GPS independent pedestrian navigation with OpenStreetMap support.
By using the internal sensors of your smartphone, SmartNavi saves up to 80%
battery compared to other navigation apps. It detects your steps and your
direction to make you independent from GPS.
.

Repo Type:git
Repo:https://github.com/Phantast/smartnavi

Build:2.0.3,24
    commit=2b9eaa1d6fca03caea4f0819755b48875445973f
    subdir=smartnavi
    gradle=free
    prebuild=sed -i -e '/play-services/d' build.gradle

Build:2.0.4 (free),25
    commit=2.0.4
    subdir=smartnavi
    gradle=free
    prebuild=sed -i -e '/play-services/d' build.gradle

Auto Update Mode:None
Update Check Mode:Tags
Current Version:2.0.4 (free)
Current Version Code:25
